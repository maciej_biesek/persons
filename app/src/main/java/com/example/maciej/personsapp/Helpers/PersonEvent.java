package com.example.maciej.personsapp.Helpers;

import com.example.maciej.personsapp.Models.Person;

public class PersonEvent {

    public final Person person;

    public PersonEvent(Person person) {
        this.person = person;
    }
}
