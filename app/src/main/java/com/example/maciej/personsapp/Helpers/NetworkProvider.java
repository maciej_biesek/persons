package com.example.maciej.personsapp.Helpers;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.ViewAnimator;

import com.bluelinelabs.logansquare.LoganSquare;
import com.example.maciej.personsapp.Adapters.PersonAdapter;
import com.example.maciej.personsapp.Models.Person;
import com.example.maciej.personsapp.R;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import static com.example.maciej.personsapp.Models.Constants.*;

public class NetworkProvider {

    private Context context;
    private OkHttpClient client;
    private PersonAdapter adapter;
    private ViewAnimator animator;

    public NetworkProvider(Context context) {
        this.context = context;
        this.client = new OkHttpClient();
    }

    public void run(final PersonAdapter personAdapter,
                    final ViewAnimator animator) throws Exception {

        this.adapter = personAdapter;
        this.animator = animator;

        Request request = new Request.Builder()
                .url(BASE_URL)
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException throwable) {
                throwable.printStackTrace();
                ((Activity) context).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        showUserDialog();
                    }
                });
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                (new AsyncGetPersons(personAdapter, animator, response.body().string())).execute();
            }
        });
    }

    private List<Person> removeDuplicates(List<Person> persons) {
        Set<Person> personsSet = new LinkedHashSet<Person>();
        personsSet.addAll(persons);
        return new ArrayList<Person>(personsSet);
    }

    public void showUserDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);

        builder.setMessage(context.getString(R.string.internet_not_found))
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        try {
                            run(adapter, animator);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });

        builder.create().show();
    }

    public class AsyncGetPersons extends AsyncTask<String, Void, Boolean> {

        private List<Person> personsList;
        private PersonAdapter adapter;
        private ViewAnimator viewAnimator;
        private String response;

        public AsyncGetPersons(PersonAdapter adapter,
                               ViewAnimator animator,
                               String response) {
            this.viewAnimator = animator;
            this.adapter = adapter;
            this.personsList = new ArrayList<Person>();
            this.response = response;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);

            adapter.setPersons(personsList);
            adapter.notifyDataSetChanged();
            viewAnimator.setDisplayedChild(1);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            personsList.clear();
        }

        @Override
        protected Boolean doInBackground(String... params) {
            try {
                List<Person> persons = LoganSquare.parseList(response, Person.class);
                personsList.addAll(removeDuplicates(persons));
            } catch (IOException e) {
                e.printStackTrace();
            }

            return true;
        }

    }
}
