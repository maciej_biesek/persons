package com.example.maciej.personsapp.Adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.maciej.personsapp.Models.Person;
import com.example.maciej.personsapp.R;

import java.util.ArrayList;
import java.util.List;

public class PersonAdapter extends BaseAdapter {

    private Context context;
    private List<Person> personsList;

    public PersonAdapter(Context context) {
        this.context = context;
        this.personsList = new ArrayList<Person>();
    }

    public void setPersons(List<Person> personsList) {
        this.personsList.clear();
        this.personsList.addAll(personsList);
    }

    @Override
    public int getCount() {
        return personsList.size();
    }

    @Override
    public Person getItem(int position) {
        return personsList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View personView;

        if (convertView == null) {
            personView = LayoutInflater.from(context).inflate(R.layout.persons_row, parent, false);
        } else {
            personView = convertView;
        }

        bindPersonToView(getItem(position), personView);

        return personView;
    }

    private void bindPersonToView(Person person, View personView) {
        TextView personName = (TextView) personView.findViewById(R.id.person_label);
        personName.setText(person.getName());
    }
}
