package com.example.maciej.personsapp.Models;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

@JsonObject
public class Person {

    @JsonField
    private String name;

    @JsonField(name="birth")
    private int yearOfBirth;

    @JsonField
    private boolean isStudent;

    public Person() {}

    public String getName() { return name; }
    public int getYearOfBirth() { return yearOfBirth; }
    public boolean getIsStudent() { return isStudent; }

    public void setName(String name) { this.name = name; }
    public void setYearOfBirth(int yearOfBirth) { this.yearOfBirth = yearOfBirth; }
    public void setIsStudent(boolean isStudent) { this.isStudent = isStudent; }

    @Override
    public boolean equals(Object  o) {
        if (o == this || !(o instanceof Person)) {
            return false;
        }
        Person p = (Person) o;
        return this.name.equals(p.name)
                && this.yearOfBirth == p.yearOfBirth
                && this.isStudent == p.isStudent;
    }

    @Override
    public int hashCode() {
        return name.hashCode() + yearOfBirth + (isStudent ? 1 : 0);
    }

    @Override
    public String toString() {
        return name + " " + yearOfBirth + " " + isStudent;
    }

}
