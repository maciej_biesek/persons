package com.example.maciej.personsapp.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import com.example.maciej.personsapp.Models.Person;
import com.example.maciej.personsapp.R;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

public class PersonDetailsFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        Person person = EventBus.getDefault().getStickyEvent(Person.class);

        View view = inflater.inflate(R.layout.fragment_details_fragment, container, false);

        TextView personName = (TextView) view.findViewById(R.id.person_name);
        TextView personYear = (TextView) view.findViewById(R.id.person_year);
        TextView personStudent = (TextView) view.findViewById(R.id.person_student);

        System.out.println(person);

        personName.setText(person.getName());
        personYear.setText(Integer.toString(person.getYearOfBirth()));

        int checkId = person.getIsStudent() ? R.string.yes : R.string.no;
        personStudent.setText(getString(checkId));

        return view;
    }
}
