package com.example.maciej.personsapp.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ViewAnimator;

import com.example.maciej.personsapp.Adapters.PersonAdapter;
import com.example.maciej.personsapp.Helpers.NetworkProvider;
import com.example.maciej.personsapp.Helpers.PersonEvent;
import com.example.maciej.personsapp.R;

import org.greenrobot.eventbus.EventBus;

public class PersonsListFragment extends Fragment {

    private PersonAdapter adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_persons_list, container, false);

        ListView listView  = (ListView) view.findViewById(R.id.persons_listview);
        ViewAnimator animator = (ViewAnimator) view.findViewById(R.id.persons_animator);
        adapter = new PersonAdapter(getContext());
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                EventBus.getDefault().post(new PersonEvent(adapter.getItem(position)));
            }
        });

        try {
            (new NetworkProvider(getContext())).run(adapter, animator);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return view;
    }



}