package com.example.maciej.personsapp.Activities;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.os.Bundle;

import com.example.maciej.personsapp.Fragments.PersonDetailsFragment;
import com.example.maciej.personsapp.Fragments.PersonsListFragment;
import com.example.maciej.personsapp.Helpers.PersonEvent;
import com.example.maciej.personsapp.R;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

public class PersonsActivity extends FragmentActivity {

    private FragmentManager fm;
    private Fragment personsListFragment;
    private Fragment personDetailsFragment;

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_persons);

        personsListFragment = new PersonsListFragment();
        personDetailsFragment = new PersonDetailsFragment();
        fm = getSupportFragmentManager();
        FragmentTransaction transaction = fm.beginTransaction();

        transaction.replace(R.id.persons_fragment, personsListFragment);
        transaction.commit();
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onEvent(PersonEvent event) {
        EventBus.getDefault().postSticky(event.person);

        FragmentTransaction transaction = fm.beginTransaction();
        transaction.replace(R.id.persons_fragment, personDetailsFragment);
        transaction.commit();
    }

    @Override
    public void onBackPressed() {
        if (personDetailsFragment.isVisible()) {
            FragmentTransaction transaction = fm.beginTransaction();
            transaction.replace(R.id.persons_fragment, personsListFragment);
            transaction.commit();
        }
        else {
            super.onBackPressed();
        }

    }
}
